const math = require('mathjs');
const bigInt = require('big-integer');
const {
    util,
    rsa_hill
} = require('./lib/rsa-hill');
const {
    performance
} = require('perf_hooks');


// const codingMatrix = math.matrix([
//     [5, 1],
//     [-1, 2]
// ]);

// let test = 'abababbaba';
// console.log(util.textToNumbers(test))
// let encrypted = rsa_hill.hillEncrypt(test, codingMatrix);
// console.log('en: ', encrypted)
// let decrypted = rsa_hill.hillDecrypt(encrypted, codingMatrix);
// console.log('de: ', decrypted)


const codingMatrix = math.matrix([
    [5, 1],
    [-1, 2]
]);

const prime1 = bigInt('39685999459597454290161126162883786067576449112810064832555157243');
const prime2 = bigInt('472772146107435302536223071973048224632914695302097116459852171130520711256363590397527');

let keyPair = rsa_hill.generateKeyPairs(prime1, prime2);

let test = 'Amedeo';

let encrypted = rsa_hill.rsaHillEncrypt(test, keyPair.public, codingMatrix);
console.log('en: ', encrypted.toString())
let decrypted = rsa_hill.rsaHillDecrypt(encrypted, keyPair.private, codingMatrix);
console.log('de: ', decrypted.toString())



// performance.mark('A');

// bigInt.prototype.modPow2 = (espbin, modu) => {
//     espbin=util.convertToBase(espbin,2)
//     let x = bigInt(0);
//     let s=bigInt(1);
//     let z;
//     let molt;
//     if (modu.isZero()) throw new Error("Cannot take modPow with modulus 0");
//     for (let i = 0; i < espbin.length; i++) {
//         if (espbin[i] === 1) {
//             let x=i
//             if (x===1){
//                 z=2
//             }
//             for (x>1){
//                 x=x/2 e=bigInt(rsa_hill.value**2).mod(modu)
//                 s=s*e
//             }
//             z = bigInt(s).mod(modu);

//         molt = bigInt(molt).multiply(z);
//     }
//     b = (molt**modu);
//     return b;
//     console.log('works')
// }

// performance.mark('B');
// performance.measure('A to B', 'A', 'B');

// let number = '3343520656666898130166902670898859139246838613870359499572299403739091870160208344754420287411757237375033967545543275343190188931850508505612315274277'

// console.log(bigInt.modPow2(bigInt(number), bigInt(number)))


// const measure = performance.getEntriesByName('A to B')[0];
// console.log(measure.duration);