const bigInt = require('big-integer');
const math = require('mathjs');
const bodyParser = require('body-parser');
const express = require('express');
const app = express();
const path = require('path');
const fs = require('fs');

const {
    util,
    rsa_hill
} = require('./lib/rsa-hill');


let userExists = (data) => {
    let userList = JSON.parse(fs.readFileSync('dati_clienti.txt', 'utf8')).users;
    for(let user of userList){
        if (JSON.stringify(user) === JSON.stringify(data)){
            return true;
        }
    }
    return false;
}

//Compilation
(() => {
    let webpack = require('webpack');
    let webpackConfig = require('./webpack.config');
    let compiler = webpack(webpackConfig);

    app.use(require("webpack-dev-middleware")(compiler, {
        logLevel: 'warn',
        publicPath: webpackConfig.output.publicPath,
        contentBase: path.join(__dirname, 'dist')
    }));

    app.use(require("webpack-hot-middleware")(compiler, {
        log: console.log,
        path: '/__webpack_hmr',
        //heartbeat: 10 * 1000,
    }));
})();


app.use('/', express.static(__dirname + '/dist'));
app.listen(3000, () => console.log('Example app listening on port 3000!'));


// create application/json parser
let jsonParser = bodyParser.json();

// create application/x-www-form-urlencoded parser
let urlencodedParser = bodyParser.urlencoded({
    extended: false
});


(async () => {
    const prime1 = bigInt('39685999459597454290161126162883786067576449112810064832555157243');
    const prime2 = bigInt('472772146107435302536223071973048224632914695302097116459852171130520711256363590397527');

    //Matrice per Hill
    const codingMatrix = math.matrix([
        [5, 1],
        [-1, 2]
    ]);

    let keyPairs = rsa_hill.generateKeyPairs(prime1, prime2);

    // POST /login gets urlencoded bodies
    app.post('/getKeyPair', urlencodedParser, function (req, res) {
        res.send(keyPairs.public)
    })

    app.post('/pay', urlencodedParser, function (req, res) {
        let data = JSON.parse(req.body.data)

        console.log('[POST] Encrypted data: ', data);

        data.strings.forEach((value, i) => {
            data.strings[i] = rsa_hill.rsaHillDecrypt(value, keyPairs.private, codingMatrix)
        })

        data.numbers.forEach((value, i) => {
            data.numbers[i] = rsa_hill.rsaDecrypt(value, ...keyPairs.private).valueOf()
        })

        console.log('[POST] Decrypted data: ', data);

        //check if user exists
        if (userExists(data)) {
            console.log('[POST] The user was found.')
            return res.sendStatus(200);
        }else{
            console.log('[POST] The user was not found.')
            return res.sendStatus(404);
        }
    });
})();