const {util, rsa_hill} = require('../lib/rsa-hill');
const math = require('mathjs');

$('.clear-btn').click(function () {
  $(this).parents('.input-group').find('input').val('')
  //console.log($(this).parent().parent().first())
})

//logger
let log = (...x) => {
  console.log(...x);
  let element = $('#output');
  x = JSON.stringify(x, null, 2).slice(1, -1)
  $('#console').append($('<p>', {}).html(x));
}

//Auto refresh for development
if (module.hot) {
  module.hot.accept();
}

//Quando il browser e' pronto e ha caricato tutta la DOM
$(async function () {
  
  //Hide banners
  $('#successo').hide();
  $('#errore').hide();
  
  //Typing effect
  let typed = new Typed('#subtitle', {
    strings: ["Welcome!", "Made with ❤️ by @kvn1351 and Nico"],
    typeSpeed: 20,
    autoInsertCss: true,
  });


  //Chiamata AJAX (HTTP POST) al server per ottenere JSON con la chiave pubblica
  //La chiave vale per 1 browser session, non verra' caricata nella cache
  log('A public key will be requested from the server...')
  const publicKeyPair = await $.post('/getKeyPair');
  log('Public Key: ' + publicKeyPair)

  //Matrice per Hill
  const codingMatrix = math.matrix([
    [5, 1],
    [-1, 2]
  ]);

  //Form handler che manda i dati criptati al server
  $('form').submit(async function (ev) {
    let output = ''
    ev.preventDefault();
    let fields = $(this).find('input')
    
    let valueDict = {
      strings: [],
      numbers: [],
    };

    for (let field of fields) {
      let value = field.value.split(' ').join('');
      //if only digits
      if (/^\d+$/.test(value)){
        valueDict.numbers.push(parseInt(value));
      }else{
        valueDict.strings.push(value);
      }
    }
    log('Unencrypted payload: ', valueDict);
    
    //Copy the object
    let encryptedDict = Object.assign({}, valueDict)


    encryptedDict.strings.forEach((value, i)=>{
      encryptedDict.strings[i] = rsa_hill.rsaHillEncrypt(value, publicKeyPair, codingMatrix)
    })

    encryptedDict.numbers.forEach((value, i)=>{
      encryptedDict.numbers[i] = rsa_hill.rsaEncrypt(value, ...publicKeyPair)
    })


    log('Encrypted payload: ', encryptedDict);
    


    log('Data will be sent to the server...')
    try{      
      await $.post('/pay', {data: JSON.stringify(encryptedDict)});
      log('Success')
      $('#successo').show();
      $('#errore').hide();
    }catch(e){
      log('Rejected')      
      $('#successo').hide();
      $('#errore').show();
    }

    /*
    setTimeout(()=>{
      window.location.replace('http://www.youtube.com/watch?v=dQw4w9WgXcQ')
    }, 3000)*/
  });
});