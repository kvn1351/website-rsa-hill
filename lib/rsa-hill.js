const bigInt = require('big-integer')
const math = require('mathjs');

// bigInt.prototype.modPow2 = (number, esp, mod) => {
//     convertToBase(esp, 2) //esp da trasformare in binario
//     for (let i = 0; i < esp.length; i++) {
//         if (i === 1) {
//             a = bigInt(number);
//             for (let times = 0; times < i.value; times++) {
//                 a = bigInt(a).pow(2).mod(mod);
//             }
//             result = bigInt(result).multiply(number).mod(mod);
//         }
//     }
//     return result;
// }

class util {
    static convertToBase(a, b) {
        let output = [];
        while (a !== 0) {
            output.push(a % b);
            a = Math.floor(a / b);
        }
        return output.reverse().join('');
    }

    static textToNumbers(text) {
        let output = [];
        let alfabet = 'zabcdefghijklmnopqrstuvwxy';
        text = text.toLowerCase();
        for (let i = 0; i < text.length; i++) {
            let char = text.charAt(i);
            output.push(alfabet.indexOf(char));
        }
        return output;
    };

    static numbersToText(array) {
        let output = '';
        let alfabet = 'zabcdefghijklmnopqrstuvwxy';
        for (let letter of array) {
            output += alfabet.charAt(letter);
        }
        return output;
    };

    static getDateAsInt() {
        return new Date().getTime();
    }

    static arrayToNumbers(array) {
        return array.join('');
    }

    static numbersToArray(numbers) {
        return numbers.toString().match(/.{1,2}/g);
    }
}

class rsa_hill {
    static hillEncrypt(input, codingMatrix) {
        input = util.textToNumbers(input);

        // if (input.length % 2 !== 0) {
        //     input.push(0);
        // }

        for (let i = 0; i < input.length; i += 2) {
            /* Example Vector for the letters B, C
            [
              [2],
              [3]
            ]
            */
            let letterPair = math.matrix([
                [input[i]],
                [input[i + 1]]
            ]);

            let encodedPair = math.multiply(codingMatrix, letterPair);

            //sets either the encoded value or its mod by 26
            input[i] = (encodedPair._data[0][0] <= 26) ? encodedPair._data[0][0] : encodedPair._data[0][0] % 26;
            input[i + 1] = (encodedPair._data[1][0] <= 26) ? encodedPair._data[1][0] : encodedPair._data[1][0] % 26;
        }
        return input;
    }

    static hillDecrypt(input, codingMatrix) {
        //const decodingMatrix = math.inv(codingMatrix);

        //math.inv doesn't work for some reason
        //I'll do it manually
        const decodingMatrix = math.matrix([
            [codingMatrix._data[1][1], codingMatrix._data[0][1] * -1],
            [codingMatrix._data[1][0] * -1, codingMatrix._data[0][0]]
        ])

        const determinant = math.det(codingMatrix);
        const euclid = rsa_hill.euclid_gcd(26, determinant);

        for (let i = 0; i < input.length; i += 2) {
            /* Example Vector for the letters B, C
            [
              [2],
              [3]
            ]
            */
            let letterPair = math.matrix([
                [input[i]],
                [input[i + 1]]
            ]);

            let encodedPair = math.multiply(decodingMatrix, letterPair);
            encodedPair = math.multiply(euclid[2], encodedPair);

            //sets either the encoded value or its mod by 26
            input[i] = 26 + (encodedPair._data[0][0] % 26);
            input[i + 1] = 26 + (encodedPair._data[1][0] % 26);
        }

        input = util.numbersToText(input);

        return input;
    }

    static generateKeyPairs(prime1, prime2) {
        let n = prime1.multiply(prime2);
        let phin = prime1.subtract(1).multiply(prime2.subtract(1));
        let e;
        do {
            e = bigInt.randBetween(300, 1000);
        } while (!(bigInt.gcd(phin, e).equals(1)));
        let d = e.modInv(phin);

        return {
            public: [n, e],
            private: [n, d],
        }
    }

    static rsaEncrypt(number, key, exponent) {
        return bigInt(number).modPow(bigInt(exponent), bigInt(key));
    }

    static rsaDecrypt(number, key, exponent) {
        return bigInt(number).modPow(bigInt(exponent), bigInt(key));
    }

    //if it's only single digit, parse it to double
    static charEncode(array) {
        return array.map((value) => {
            if (value < 10) value += 40;
            return value;
        });
    }

    static charDecode(array) {
        return array.map((value) => {
            if (value >= 40) value -= 40;
            return value;
        });
    }

    //There's a bug in chrome that makes it impossible to use the Hill functions
    //Math.js multiply doesn't work as intended...
    //Todo: find a workaround
    static rsaHillEncrypt(message, keypair, hillMatrix) {
        //let messageArray = rsa_hill.hillEncrypt(message, hillMatrix);
        //messageArray = rsa_hill.charEncode(messageArray);
        let messageArray = rsa_hill.charEncode(util.textToNumbers(message));
        let number = util.arrayToNumbers(messageArray);
        return rsa_hill.rsaEncrypt(number, ...keypair);
    }

    static rsaHillDecrypt(message, keypair, hillMatrix) {
        let number = rsa_hill.rsaDecrypt(message, ...keypair);
        let messageArray = util.numbersToArray(number);
        messageArray = rsa_hill.charDecode(messageArray);
        //return rsa_hill.hillDecrypt(messageArray, hillMatrix);
        return util.numbersToText(messageArray);
    }

    //Function taken from https://www.w3resource.com/javascript-exercises/javascript-math-exercise-47.php
    static euclid_gcd(a, b) {
        a = +a;
        b = +b;
        if (a !== a || b !== b) {
            return [NaN, NaN, NaN];
        }

        if (a === Infinity || a === -Infinity || b === Infinity || b === -Infinity) {
            return [Infinity, Infinity, Infinity];
        }
        // Checks if a or b are decimals
        if ((a % 1 !== 0) || (b % 1 !== 0)) {
            return false;
        }
        let signX = (a < 0) ? -1 : 1,
            signY = (b < 0) ? -1 : 1,
            x = 0,
            y = 1,
            u = 1,
            v = 0,
            q, r, m, n;
        a = Math.abs(a);
        b = Math.abs(b);

        while (a !== 0) {
            q = Math.floor(b / a);
            r = b % a;
            m = x - u * q;
            n = y - v * q;
            b = a;
            a = r;
            x = u;
            y = v;
            u = m;
            v = n;
        }
        return [b, signX * x, signY * y];
    }
}


module.exports = {
    util,
    rsa_hill,
}