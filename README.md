# README #

## Compito di Informatica (2018)

### Pseudo credit card payment site.

Made with NodeJS, HotReloader, Webpack, Babel, Bootstrap 4, ExpressJS, mathJS, bigIntegerJS

* `Webpack` compiles all .js files into a single main.js file that can be used in a browser.
* `Babel` transpiles modern js into older standards
* `Bootstrap 4` is a html framework that offers predefined CSS rules
* `ExpressJS` handles http communication

## Installation
#### Windows/macOS/Linux (ARM raspberry inclusive)
1. Install `node`, `yarn` and `git`
2. Open a terminal or command line and `clone` this repo (`git clone https://kvn1351@bitbucket.org/kvn1351/website-rsa-hill.git`)
3. Enter the repo's directory: `cd *website-rsa-hill*`
4. Install the dependencies: type `yarn`
5. To run the webserver type `yarn run main:host`. This will host the server on `kvn1351.localtunnel.me` and `localhost:3000`
6. Other commands include: `yarn run main` (to run on localhost without localtunnel) and `yarn run build` to compile the files into `/dist`.

## Libraries used
* `rsa_hill` : Self created library (100% from scratch besides the GCD algorithm)
* `big-integer` : https://www.npmjs.com/package/big-integer
* `mathjs`: http://mathjs.org
* `bootstrap 4` (html and css framework): https://getbootstrap.com

## Useful documentation
* https://www.khanacademy.org/computing/computer-science/cryptography/modern-crypt
* https://www.pagedon.com/rsa-explained-simply/programming
* https://www.robinwieruch.de/linear-algebra-matrix-javascript/


## License

MIT


© @kvn1351